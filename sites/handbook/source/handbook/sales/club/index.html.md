---
layout: handbook-page-toc
title: "GitLab President's Club"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

President’s Club is synonymous with excellence and rewards individuals for achieving specific goals. This event aligns with our values of Collaboration and Results which helps to attract high-performing Sales Reps and other roles that support the sales efforts and allow the company to grow at the expected rate. At GitLab, this reward is in the form of an incentive trip to celebrate the success of our top Field Sales Professionals and others in the organization who helped support the team throughout the year.

![tropical-destination](/handbook/sales/club/tropical2.jpg)

## How to Qualify

For GitLab's FY21 President's Club, 42 GitLab team members (along with 1 guest per team member) will be rewarded with an invitation to join the President's Club incentive trip in May 2021. Team member start dates must be on or before 2020-08-03 to be eligible and still be employed by GitLab at the time of the incentive trip to receive the benefit. Selection criteria varies by role (see below).

## Selection Criteria

| Category | # of winners | Criteria | 
| ------ | ------ | ------ |
| Top Strategic Account Leaders | 8 | based on % of annual full quota (full quota, non-ramped annual quota, prorated based on start date) as shown on the [FY21 President's Club Dashboard](https://app.periscopedata.com/app/gitlab/670929/WIP:-FY21-President's-Club-Dashboard) |
| Top Mid Market Account Executives | 4 | same as above |
| Top SMB Account Executive | 2 | VP/CRO selection (see SMB tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Area Sales Managers | 3 (2 ENT, 1 COM) | based on top quota performance (full quota, non-ramped annual quota, prorated based on start date) as shown on the [FY21 President's Club Dashboard](https://app.periscopedata.com/app/gitlab/670929/WIP:-FY21-President's-Club-Dashboard) |
| Top Enterprise Sales Regional Director | 1 | same as above |
| Top Solution Architects | 8 (includes Channel SAs) | VP/CRO selection (see CS tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Inside Sales Rep | 1 | VP/CRO selection (see ISR tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Channel & Alliances Managers | 3 (2 Channel Sales Managers, 1 Biz Dev/Alliances Manager) | VP/CRO selection (see Channels/Alliances tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Technical Account Managers & Professional Services Engineer | 5 (4 TAMs, 1 PSE) | VP/CRO selection (see CS tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Sales Development Reps | 2 | CMO selection (see SDR tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top Sales Development Manager | 1 | CMO selection (see SDR tab of [this doc](https://docs.google.com/spreadsheets/d/1UMY0rDbSPjw_X9nXKpe7r1SMPuZohgVjVhqEpdGYBJQ/edit?usp=sharing) for details) |
| Top CRO Organization Managers | 2 managers in the CRO org that don’t fall under one of the other Club award categories | VP/CRO selection (sales leader nominations to be collected by 1/31/21) |
| Non-Sales MVPs | 2 | VP/CRO selection (sales leader nominations to be collected by 1/31/21) |

## Additional Criteria
- Sales management must have a total of 36 SAL months under management during the fiscal year to qualify (e.g. 1 SAL all year would be 12 SAL months). 

Please direct any questions to your manager.

## Club 2021 FAQ

**What is the latest on President’s Club 2020?**

We continue to adapt to the global health and travel challenges, and after a lot of careful consideration, we have made the decision to postpone the 2020 Presidents Club until next year. [2020 winners](https://docs.google.com/presentation/d/1XnXGkNagz6ZbJIqzBphoXdAs-Vra59Kjrj4IT_YEyp4/edit#slide=id.g883d59c798_4_0) will be combined with 2021 winners in the same event. 

**What are the dates and location of President’s Club 2021?**

[President’s Club 2021](/handbook/sales/club/) will be at the [Ritz Carlton in Maui, Hawaii](https://www.ritzcarlton.com/en/hotels/kapalua-maui) from May 11-14, 2021.  

**What happens if I am a President’s Club recipient in both 2020 and 2021?**

If you are a President’s Club winner in <ins>both 2020 and 2021</ins>, you will receive two extra days at the Ritz Carlton in Maui for you and your guest. 

**Do I get extra time if I’m traveling from EMEA/APAC?**

While we understand that the trip is long, there will not be extra time allotted to team members traveling from EMEA or APAC. However, you’re always welcome to book with our rate up to 3 days pre- and/or post-event on your own.  

**How do I qualify for President’s Club?**

Please see the [How to Qualify](/handbook/sales/club/#how-to-qualify) section above. 

**How are President’s Club winners selected?**

Please see the [Selection Criteria](/handbook/sales/club/#selection-criteria) section above. 

**What is the schedule for President’s Club?**

- May 11 - Travel Day & Welcome Reception
- May 12 - Planned activities; Club Dinner
- May 13 - Planned activities; Farewell Reception
- May 14 - Travel Day; two-time winners (2020 and 2021) will stay 
- May 15 - Relaxation day for two-time winners 
- May 16 - Travel Day for two-time winners

**If I am selected as a President’s Club winner, can I bring a guest?**

Yes! Your significant other or guest is invited (and encouraged!) to attend Club with you. Please note that guests must be at least 21 years of age. Club is something truly special – a time for us to step back and celebrate your incredible hard work and accomplishments – and we want your guest to be a part of that! 

**Is my guest’s travel covered by GitLab?**

Yes, this trip will be all-inclusive for both you and your guest. Travel, hotel, meals, and activities will be covered by GitLab.

**Can I bring my kids to Club?**

Club is an adult-only event that is intended for team members to spend time together, celebrate, and build relationships on the foundation of their success and contribution to the company. 

If you have a family member or child that requires 24 hour care, or some other unique circumstance that jeopardizes your ability to attend, we can explore exceptions. For additional information, please reach out to Cheri Holmes and Libby Schulze via email. 

**When will we announce Club 2021 winners?**

Winners for President’s Club 2021 will be announced during the Awards Dinner at Sales Kickoff (SKO) taking place from Feb 8-12, 2021. For more information about SKO, please see [this Handbook page](/handbook/sales/training/SKO/).

Reference: [GitLab Events Code of Conduct](https://about.gitlab.com/company/culture/contribute/coc/)

