---
layout: handbook-page-toc
title: Product Investment
---

This page is meant to provide insight into how we consider and allocate our investment across our [Product Hierarchy](/handbook/product/product-categories/#hierarchy).

## Investment by Stage

### DevOps Stage Scoring Framework

We have a framework we use to assess the investment level for each DevOps stage.  The three investment drivers are as follows:

1. Product Usage Driver Score is a measure of the number of active users per month for that stage
2. Revenue Driver score is a measure of the ability of that stage to increase revenue through higher ASP or seat adds
3. SAM (Served Addressable Market) Driver score is a measure of the size of the DevOps market that stage enables GitLab to serve over the next three years

Each driver is scored on a 1-5 basis.  Here is a description of how each driver is scored.

| Score | Usage Driver Key | Revenue Driver Key | SAM Driver Key |
| ----- | ---------------- | ------------------ | -------------- |
| 1 | <10k SMAU | Not a driver of paid sales | <200m SAM over the next three years |
| 2 | 10k-50k SMAU | Used by paid customers, but not a major element in paid sales | 200m-500m SAM over the next three years |
| 3 | 50k-100k SMAU | Key element in Starter+ sales | 500m-1B SAM over the next three years |
| 4 | 100k-200k SMAU | Key element in Premium+ sales | 1B-2B SAM over the next 3 years |
| 5 | >200k SMAU | Key element in Ultimate sales | >2B SAM over the next three years |

### Enabling Stage Scoring Framework

We score enabling groups differently. Their Criticality of Use score replaces their Usage score and their Degree of Enablement replaces their SAM Score. Enabling groups utilize the same scoring methodology as DevOps stage groups for their Revenue Driver score.

1. Criticality of Use score is a measure of how critical the enabling categories are to the usage of GitLab. 
2. Degree of Enablement score is a measure of much the enabling categories expand the enablement of GitLab to the DevOps market in terms of percent of the SAM it enables.

As for DevOps stage scores - each driver is scored on a 1-5 basis.  Here is a description of how each driver is scored.

| Score | Criticality of Use | Degree of Enablement |
| ----- | ---------------- | ------------------ |
| 1 | Non-essential to use of GitLab |  Enables < 25% of the DevOps Market|
| 2 | Non-essential but becoming more critical | Enables > 25% of the DevOps Market |
| 3 | Essential but not required | Enables >50% of the DevOps Market |
| 4 | Heavily recommmended for use of GitLab or GitLab.com | Enables >70% of the DevOps Market |
| 5 | Required for use of GitLab or GitLab.com | Enables entire DevOps Market |

### Scoring Results

The following table provides scores for each of our DevOps stages and a comparison of the total score to our current development spend allocation as defined by the percentage of total individual contributor developers in product groups.

| Stage | Usage Driver Score | Revenue Driver Score | SAM Driver Score | Combined Score | % of Total Score | Stage Development Spend % | Score/Spend Difference |
|-|-|-|-|-|-|-|-|
<% # this calculates the total score across all stages needed to calculate the percent of total score %>
<% totalScore = 0 %>
<% totalDevelopers = 0 %>
<% data.sections.each do |sectionKey, section| %>
  <% data.stages.stages.select{|stageKey,stage| stage.section==sectionKey}.each do |stageKey, stage| %>
    <% totalScore += stage.usage_driver_score.to_i + stage.revenue_driver_score.to_i + stage.sam_driver_score.to_i %>
    <% stage.groups.each do |groupKey, group| %>
      <% totalDevelopers += group.group_developers if group.group_developers %>
    <% end %>
  <% end %>
<% end %>
<% data.sections.each do |sectionKey, section| %>
  <% data.stages.stages.select{|stageKey,stage| stage.section==sectionKey}.each do |stageKey, stage| %>
    <% next if !stage.marketing %>
    <% stage_devs = 0 %>
    <% stage.groups.each do |groupKey, group| %>
      <% stage_devs += group.group_developers if group.group_developers %>
    <% end %>
    <% stageLine=[] %>
    <% combinedScore = stage.usage_driver_score.to_i + stage.revenue_driver_score.to_i + stage.sam_driver_score.to_i %>
    <% next if combinedScore == 0 %>
    <% percent_of_total = (combinedScore.to_f/totalScore.to_f)*100 %>
    <% percent_of_developers = (stage_devs.to_f / totalDevelopers.to_f)*100 %>
    <% difference = percent_of_developers - percent_of_total%>
    <% stageLine << "#{stage.display_name}"%>
    <% stageLine << stage.usage_driver_score ? "#{stage.usage_driver_score}" : "0" %>
    <% stageLine << stage.revenue_driver_score ? "#{stage.revenue_driver_score}" : "0"%>
    <% stageLine << stage.sam_driver_score ? "#{stage.sam_driver_score}" : "0" %>
    <% stageLine << "#{combinedScore}" %>
    <% stageLine << "#{percent_of_total.round(1)}%" %>
    <% stageLine << "#{percent_of_developers.round(1)}%" %>
    <% stageLine << "#{"+" if difference > 0}#{difference.round(1)}%" %>
    <%= "| #{stageLine.join(' | ')} |" if stageLine %>
  <% end %>
<% end %>

## Investment by Group

*Note: Enablement and Growth stages and groups aren't included, as they don't have specific target markets, and don't have a dedicated SAM.  Therefore, they can't be compared directly with feature-function stages and aren't included in this analysis.*

| Section: Stage: Group | Usage Driver | ASP Driver | SAM Driver | Total | % of Total | Group Development Spend % | Score/Spend Difference |
|-|-|-|-|-|-|-|-|
<% # this calculates the total score across all stages needed to calculate the percent of total score %>
<% totalScore = 0 %>
<% data.sections.each do |sectionKey, section| %>
  <% data.stages.stages.select{|stageKey,stage| stage.section==sectionKey}.each do |stageKey, stage| %>
    <% next if !stage.marketing %>
    <% totalScore += stage.usage_driver_score.to_i + stage.revenue_driver_score.to_i + stage.sam_driver_score.to_i %>
  <% end %>
<% end %>
<% totalScore = 0 %>
<% data.sections.each do |sectionKey, section| %>
  <% data.stages.stages.select{|stageKey,stage| stage.section==sectionKey}.each do |stageKey, stage| %>
    <% stage.groups.each do |groupKey, group| %>
      <% totalScore += group.usage_driver_score.to_i + group.asp_driver_score.to_i + group.sam_driver_score.to_i %>
    <% end %>
  <% end %>
<% end %>
<% data.sections.each do |sectionKey, section| %>
  <% data.stages.stages.select{|stageKey,stage| stage.section==sectionKey}.each do |stageKey, stage| %>
    <% stage.groups.each do |groupKey, group| %>
      <% combinedScore = group.usage_driver_score.to_i + group.asp_driver_score.to_i + group.sam_driver_score.to_i %>
      <% next if combinedScore == 0 %>
      <% percent_of_total = (combinedScore.to_f/totalScore.to_f)*100 %>
      <% percent_of_developers = (group.group_developers.to_f / totalDevelopers.to_f)*100 %>
      <% difference = percent_of_developers - percent_of_total %>
      <% groupLine=[] %>
      <% # groupLine << "#{section.name} : #{stage.display_name}: #{group.name}" %>
      <% groupLine << "[#{section.name}: #{stage.display_name}: #{group.name}](/handbook/#{group.group_link})" if group&.group_link %>
      <% groupLine << group.usage_driver_score ? "#{group.usage_driver_score}" : "" %>
      <% groupLine << group.asp_driver_score ? "#{group.asp_driver_score}" : "" %>
      <% groupLine << group.sam_driver_score ? "#{group.sam_driver_score}" : "" %>
      <% groupLine << "#{combinedScore}" %>
      <% groupLine << "#{percent_of_total.round(1)}%" %>
      <% groupLine << "#{percent_of_developers.round(1)}%" %>
      <% groupLine << "#{"+" if difference > 0}#{difference.round(1)}%" %>  
      <%= "| #{groupLine.join(' | ')} |" if groupLine %>
    <% end %>
  <% end %>
<% end %>


## Changes

Changes to this page are made by adding new values to the single sources of truth ([categories.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/categories.yml) and [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml)) and then editing this page in order to display those values in table form. Be sure the definition for any column value is defined in [stage](/handbook/marketing/website/#stage-attributes) and [category attributes](/handbook/marketing/website/#category-attributes) and that the column headers link to those definitions.

The process for making changes requires you to submit a merge request to the [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) for the specific keywords that have changed and provide the data source for that change. Assign the merge request to the EVP or VP of Product and slack `#product` for approval/merge. You can reference the [Release Stage update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/62103) for an example.
    
