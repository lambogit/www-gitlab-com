---
layout: markdown_page
title: "Redhat Openshift"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
OpenShift Container Platform by Red Hat, is an on-premises private platform as a service product, built around a core of application containers powered by Docker, with orchestration and management provided by Kubernetes, on a foundation of Red Hat Enterprise Linux.  It allows developers to deploy applications using a library of supported technologies that include Java, Node.js, .NET, Ruby, Python, PHP and more.

With every handoff in IT, there is yet another opportunity for friction to slow the business and prevent innovation from reaching your customers. Because GitLab supports the entire delivery lifecycle and can deploy and manage your applications for you in Kubernetes, your teams can dramatically accelerate their delivery cadence and meet the expectations of the business. GitLab AutoDevOps can dramatically accelerate the time it takes to get a new application and a new pipeline up and running. Gitlab is complimentary with Openshift as a Kubernetes or Docker platform, and is easy to deploy using these technologies.

## Resources
* [Redhat Openshift](https://www.openshift.com/)
* [Redhat Openshift Wikipedia](https://en.wikipedia.org/wiki/OpenShift)
